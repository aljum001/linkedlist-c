/* Mohamed Aljumaily Assignment 4. LL.cpp
This program will hold all of the function
definition that were declared for class
LL in the LL.h file. This file will be
modifiying, adding and working with nodes
in the linked list. */


#include "Node.h"
#include "LL.h"
#include <iostream>
#include <string>
using namespace std;

LL :: LL()
{ //default constructer sets head to null
  head = nullptr;
}

void LL :: readFromArrays(string nArr[], string pArr[], int size)
{//expects 2 arrays and their size as a paramater
  for (int i = 0; i < size; i++)
    { //for loop to read frfom an array and call
      append(nArr[i] , pArr[i]); //append func
    }
}

void LL :: append(string pName, string phone)
{//append function will create a pointer
 //and have it hold the address of a dynamiclly
 //allocated node
  Node *newNode = new Node;
  //inputting the name and phone number into the node
  newNode -> name = pName;
  newNode -> phoneNumber = phone;
  if(!head)
    { //if the list is empty make the newnode the first
      head = newNode;
    }
  else
    { // else make a new pointer to traverse the list
      Node *nodePtr = head;
      while (nodePtr ->next)
	{ //loop to traverse the list.
	  nodePtr = nodePtr ->next;
	}
      //adding new node to the end of the list
      nodePtr ->next = newNode;
    }



}
void LL :: insert(string pName, string phone)
{ //insert function creates a pointer and have it
  //hold the address of the new Node with the values
  //from the paramater
  Node *newNode = new Node;
  //adding values to the node.
  newNode -> name = pName;
  newNode -> phoneNumber = phone;
  if (!head)
    { //if the list empty make the new Node the 
      head = newNode;
    } //first node
    else 
      {//make 2 pointers a current and previous one to 
	//traverse the list
      Node *nodePtr = head;
      Node *previousNode = nullptr;
      while(nodePtr!=NULL && (nodePtr->name < pName))
	{ //while loop with the condition that the name
	  //of the current is less than the name you're
	  //trying to find and nodePtr is not null
	  //traverse the list with the pointers
	  previousNode = nodePtr;
	  nodePtr = nodePtr -> next;
	}
      if(!previousNode)
	{ //if the while loop did not start
	  //insert at the beginning and make
	  //the new node point to the next node
	  head = newNode;
	  newNode->next = nodePtr;
	}
      else
	{
	  //else insert at the correct poisition
	  //connect previous to the new node and
	  //the new node to the next node
	  previousNode ->next = newNode;
	  newNode ->next = nodePtr;
	}
    }
}

void LL :: insertAtPos(string pName, string phone, int pos)
{//this function inserts a new node at a specific position
  //creates a pointer and has it hold the value of a new Node
  Node *newNode = new Node;
  newNode -> name = pName;
  newNode -> phoneNumber = phone; 
  if (pos == 1)
    { //if the position is the first one
      // insert the new node in the first spot
      //and have head point to the new node
      newNode ->next = head;
      head = newNode;
    }
  else
    {
      // else make 2 pointers a previous and a current to
      //traverse the list
     Node *nodePtr = head;
     Node *prevPtr = nullptr;
     int counter = 1; // counter to the condition
      while (nodePtr && counter != pos)
	{ //while nodePtr is pointing to something
	  //and counter is not equal to the position
	  prevPtr = nodePtr; //traverse the list
	  nodePtr = nodePtr -> next;
	  counter++; // increment counter
	}
      if (!nodePtr)
	{ //if the position was not found
	  //insert the new Node at the end
	  prevPtr ->next = newNode;
	}
      else
	{ //insert the new node in the position
	  //prevptr points to the newNode and
	  //newNode points to the next node
	  prevPtr ->next = newNode;
	  newNode -> next = nodePtr;
	}
    }
}

void LL :: print() const
{ //print functions prints the contents of the list
  if (!head)
    { //if the list is empty print below
      cout << "List is empty." << endl;
    }
  else
    { //counter for the position of the Node
      int counter = 1;
      //pointer to point at the start of the list
      Node *nodePtr = head;
      while (nodePtr)
	{ //loop while nodePtr is pointing to something
	  //print the contents below
	  cout << counter << ". " << nodePtr ->name << ", "
	       << nodePtr -> phoneNumber << endl;
	  counter++; // increment counter
	  //traverse the list
	  nodePtr = nodePtr ->next;
	}
    }
}

void LL :: printReverse()
{//print reverse calls a new function with 
 //the head pointer of the current list
  //to print it in reverse
  //head is a private attribute so it cannot be
  //called from main
  printReverseNode(head);
}

void LL :: printReverseNode(Node *head)
{// if head reaches null the return back
  if (!head)
    {
      return;
    }
  else
    { //else call the function recursively with the next value of 
      //head
      printReverseNode(head ->next);
      //print the value once the function returns
      cout << head -> name << " " << head -> phoneNumber << endl;
    }        
}

void LL :: searchByName(string pName)
{//searchbyname tries to find if a person exists
  //in the node with a specific name and prints
  //out the number of the node
  if (!head)
    {//if the list is empty print below
      cout << "The list is empty." << endl;
    }
  else
    { //make a pointer poin tto the beginning of the list
      Node *nodePtr = head;
      int counter = 1; //counter for the node #
      while(nodePtr && (pName != nodePtr ->name))
	{ //while loop checking if nodeptr exists and
	  //the name has not been found yet
	  nodePtr = nodePtr -> next; 
	  //traverse the list and increment counter
	  counter++;
	}
      if (!nodePtr)
	{ //if the person is not found print below
	  cout << "That person does not exist." << endl;
	}
      else
	{ //if the person is found print the counter
	  cout << "The number of the Node holding " << pName
	       << " is: " << counter << endl;
	}
    }
}

void LL :: updateNumber(string pName, string newPhone)
{ //this function finds a person in the list and updates
  //their phonenumber
  if (!head)
    { //if t he list is empty print below
      cout << "The list is empty." << endl;
    }
  else
    { //else make a ptr that points to the beginning of the list
      Node * nodePtr = head;
      while(nodePtr && (pName != nodePtr ->name))
	{ //while the ptr exists and the name has not been found
	  nodePtr = nodePtr -> next;
	  //traverse the list
	}
      if (!nodePtr)
	{ //if the name is not found in the list print below
	  cout << "The person you're looking for is not on the list."
	       << endl;
	}
      else
	{ //else update the number of the person in the node
	  nodePtr -> phoneNumber = newPhone;
	}
    }
}

void LL :: removeRecord(string pName)
{ //removerecord function finds a person in the lsit
  //and deletes that nodes and maintain the order
  //of the list
  Node *nodePtr = nullptr;
  if (!head)
    { //if the list is empty print below
      cout << "The list is empty." << endl;
    }
  if (head -> name == pName)
    { //if the first element is the one that needs 
      //to be removed 
      //delete first node and connect to the next one
      nodePtr = head ->next;
      delete head;
      head = nodePtr;
    }
  else
    { //else make 2 pointers one pointing to head
      nodePtr = head;
      Node *prevPtr = nullptr;
      while(nodePtr && (pName != nodePtr -> name))
	{ //while the ptr exists and the name has not been found
	  prevPtr = nodePtr;
	  nodePtr = nodePtr -> next;
	  //traverse the list with both ptrs
	}
      if (nodePtr)
	{ //if the name was found delete the node
	  //and connect the remaining nodes
	  prevPtr ->next = nodePtr->next;
	  delete nodePtr;
	}
      else
	{ //if it was not found print below.
	  cout << pName <<  " does not exist." << endl;
	}
    }
}

LL :: ~LL()
{//destructor calls the destroy function to 
  destroy(); //destroy the list
}

void LL :: destroy()
{ //destroy function destroys all the nodes in the list 
  if (!head)
    { //if the list is already empty print below
      cout << "List is empty." << endl;
    }
  else
    {//else make a ptr
      Node *nodePtr = nullptr;
      while(head)
	{//while there are nodes in the list
	  nodePtr = head;
	  head = head ->next;
	  delete nodePtr;
	  //delete each node and move on to the next
	}
    }
}

LL :: LL(const LL& source)
{ //the copy constructor makes a deep copy of a list
  //into a new one. Only accepts a list sent in by 
  //reference.
  if(!source.head)
    {//if the list the copy is being made from is empty print.
      cout << "The list you're copying from is empty." << endl;
    }
  else
    {//else make a ptr point to the copy list head.
      //and make the new head point to null
      head = nullptr;
      Node *nodePtr = source.head;
      while(nodePtr)
	{//while the copy list is not at null.
	  //call the append function with the new list
	  //to add nodes and then traverse the copy list.
	  append(nodePtr -> name, nodePtr -> phoneNumber);
	  nodePtr = nodePtr ->next;
	}
    }
}

LL LL :: operator=(const LL& source)
{ //the operator = function overloads the assignement 
  //operator to perform a deep copy of one list to the other
  //when they are assigned to each other.
  //the list that is being copied to need to be empty
  //so the destroy function is called
  destroy();
  if (!source.head)
    { // if the copy list is empty print below
      cout << "The list you're assigning from is empty." << endl;
    }
  else
    { //else make a ptr point to t he copy list
      Node *nodePtr = source.head;
      while(nodePtr)
	{//while the copy list is not pointing to null
	  //call the append function with the new list 
	  //to append nodes to it. and traverse the copy list
	  append(nodePtr -> name, nodePtr -> phoneNumber);
	  nodePtr = nodePtr ->next;
	}
      return *this; //using the THIS pointer to return 
      //the calling object which is the copied list.
    }
}

void LL :: reverse()
{//the reverse function reverses a list so that the last node
 //becomes the first
  if (!head)
    {//if the list is empty print below
      cout << "The list you are trying to reverse is empty." << endl;
    }
  else
    {//else make 2 pointers and one pointing to head
      Node *nodePtr = head;
      Node *prevPtr = nullptr;
      while(nodePtr)
	{//while nodeptr is not null
	  if (nodePtr == head)
	    { //if nodeptr is pointing to the first node
	      //make it's next pointer point to null and
	      //traverse the list
	      nodePtr = nodePtr->next;
	      head->next = nullptr;
	    }
	  else
	    {//else make prevpointer point to nodePtr
	      prevPtr = nodePtr;
	      //traverse the list with nodePtr
	      nodePtr = nodePtr ->next;
	      //make the Node connect to the node head is
	      //pointing to
	      prevPtr ->next = head;
	      //make head point to the new Node which is now
	      //the first Node.
	      head = prevPtr;
	    }
	}
    }
}


