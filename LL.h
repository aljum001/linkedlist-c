/*Mohamed Aljumaily Assignemnt 4. LL.h file.
This program will act as the connection between
each node in the linked list and will have 
multiple functions modifying and adding and 
working with each node in the list.
LL.h will be the class decleration
of LL */
#ifndef LL_H
#define LL_H
#include "Node.h"
#include <string>
using namespace std;

class LL
{
  //private attributes will have a pointer
  //head that will point to the beginning
  //of the linkeed list
 private: 
  Node *head;
  void printReverseNode(Node *head);
 public:
  LL(); //default constructer
  //read from arrays will read from an array
  //into a node
  void readFromArrays(string[], string[], int);
  //append will append a function to the list
  void append(string, string);
  //insert will insert a Node and maintain the 
  //order
  void insert(string, string);
  //insertatPos will insert a node a a specific position
  void insertAtPos(string, string, int);
  //print will print out the linked list contents
  void print() const;
  //printsReverse prints the content in reverse order
  void printReverse();
  //searchbyname searches for a node with a specific name
  //to see if it exists
  void searchByName(string);
  //updatenumber will update a specific person's number
  //if they exist
  void updateNumber(string, string);
  //RemoveRecord will remove a Node if the person exists
  void removeRecord(string);
  //destructor
  ~LL();
  //deestroy will destroy the whole linked list
  void destroy();
  //copy cunstructor performs a deep copy of the list
  LL(const LL&);
  // overloaded assignement operator
  LL operator=(const LL&);
  //reverse will reverse a linked list
  void reverse();
};

#endif
