/*Mohamed Aljumaily Assignemnt 4. Node.cpp file will
hold the function definitions for the class Node.
It just consists of the default and overloaded
constructers for each person of the directory
 */
#include "Node.h"
#include <iostream>
#include <string>
using namespace std;

Node :: Node()
{ //default constructer setting 
  //pointer next to null and both strings to empty.
  next = nullptr;
  name = "";
  phoneNumber = "";
}

Node :: Node(string n, string pN)
{ //overloaded constructer sets pointer next
  //to null and name to n and phoneNumber to pN
  next = nullptr;
  name = n;
  phoneNumber = pN;
}

