/* Mohamed Aljumaily Assignment 4. 
This program will create a linked list of 
a phone directory and will hold the names
and phone numbers of each person
Node.h file will be declaration for Node which 
will hold private attributes and some 
public functions to modify them */
#ifndef NODE_H
#define NODE_H
#include <string>
using namespace std;

class Node
{ //assigning class LL as afriend to have
  //direct access to Node's attributes
  friend class LL;
 private:
  //holds 2 strings for name and phoneNumber
  //and a pointer that will point to the
  //next Node
  string name;
  string phoneNumber;
  Node *next;
 public:
  //default and overloaded constructers
  Node();
  Node(string, string); // expects 2 paramaters


};

#endif
